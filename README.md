# Static Webapp + Gulp Starter

This is a tweaked version of the Yo generator, named generator-gulp-webapp.

The purpose of this project is to have a foundation on which to build static apps with a build toolchain. After spending hours of configuring a starter from scratch, Yo came to the rescue.  After evaluating several Yo generators for static-based projects, the generator-gulp-webapp was picked.

### Additions

The following extras were added to this stater:

- Bootstrap 4 alpha.2
- Font-awesome 4.5

### How to use

`gulp serve` to use localhost dev server including browser refresh

`gulp build` to build the source directory files into /dist folder

`gulp serve:dist` to run a server using the /dist folder contents
